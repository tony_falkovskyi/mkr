<?php


class Player
{
    private $fio;
    private $age;
    private $amplua;

    /**
     * Player constructor.
     * @param $fio
     * @param $age
     * @param $amplua
     */
    public function __construct($fio, $age, $amplua)
    {
        $this->fio = $fio;
        $this->age = $age;
        $this->amplua = $amplua;
    }



    /**
     * @return mixed
     */
    public function getFio()
    {
        return $this->fio;
    }

    /**
     * @param mixed $fio
     */
    public function setFio($fio)
    {
        $this->fio = $fio;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @return mixed
     */
    public function getAmplua()
    {
        return $this->amplua;
    }

    /**
     * @param mixed $amplua
     */
    public function setAmplua($amplua)
    {
        $this->amplua = $amplua;
    }




}