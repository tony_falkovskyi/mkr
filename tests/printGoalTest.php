<?php

use phpDocumentor\Reflection\Types\Iterable_;
use \PHPUnit\Framework\TestCase;
include 'Team.php';
class printGoalTest extends TestCase
{
    private $team;

    protected function setUp() : void
    {
        $this->team = new Team();
    }

    public function testGoalKeeper(){
        $goal = $this->team->printGoalKeeper();
        $this->assertContains("goal", $goal);
    }

    /**
     * @param $age
     * @param $expected
     * @dataProvider addDataProvider
     */
    public function testYoungPlayers($age, $expected){
        $result = $this->team->countPlayerYounger($age);
        $this->assertEquals($expected, $result);
    }

    public function addDataProvider(){
        return array(
            array(21, 5),
            array(19, 3),
            array(28, 6),
        );
    }

    protected function tearDown() : void
    {
        isset($this->team);
    }


}